require 'spec_helper'
require './lib/Bibliografia.rb'

describe Biblioteca::Bibliografia do
  before :all do
    @b = Biblioteca::Bibliografia.new
  end
  
  describe "Gestion de autores" do
    it "Añadir autores" do
      @b.add_author("Carl Sagan")
      @b.add_author("Neil De Grasse Tyson")
    end
    
    it "Recuperar autores" do
      @b.Author[0].should eq("Carl Sagan")
      @b.Author.should eq(["Carl Sagan","Neil De Grasse Tyson"])
    end
  end
  describe "Gestion de títulos" do
    it "Añadir Título" do
      @b.add_title("El mundo y sus demonios")
    end
    
    it "Recuperar título" do
      @b.Title.should eq("El mundo y sus demonios")
    end
  end
  
  describe "Gestion de serie" do
    it "Recuperar serie" do
      @b.Serie.should eq (nil)
    end
  
    it "Añadir serie" do
      @b.add_serie("Divulgación científica ULL")
    end
    
    it "Recuperar serie" do
      @b.Serie.should eq ("Divulgación científica ULL")
    end
  end
  
  describe "Gestión de editorial" do
    it "Añadir editorial" do
      @b.add_editorial("Anaya")
    end
    
    it "Recuperar editorial" do
      @b.Editorial.should eq ("Anaya")
    end
  end
  
  describe "Gestión de edición" do
    it "Añadir número de edición" do
      @b.set_edicion(23)
    end
    
    it "Recuperar edición" do
      @b.Edicion.should eq(23)
    end
  end
  
  describe "Gestión de fecha de publicación" do
    it "Establecer fecha de publicación" do
      @b.set_publication("23-AGO-1994")
    end
    
    it "Recuperar fecha de publicación" do
       @b.Fecha_Publication.should eq("23-AGO-1994")
     end
   end
   
  describe "Gestión de números ISBN" do
    it "Recuperar número(s) ISBN" do
      @b.ISBN.should eq([])
    end
    
    it "Añadir número ISBN" do
      @b.add_ISBN([10,66666])
      @b.add_ISBN([12,77777])
    end
    
    it "Recuperar número(s) ISBN" do
      @b.ISBN.should eq([[10,66666],[12,77777]])
    end
  end
  
  describe "Referencia con Formato" do
    it "Recuperar Formato" do
      @b.get_formato.should eq("Carl Sagan, Neil De Grasse Tyson"+"\n"+
                               "El mundo y sus demonios"+"\n"+
                               "Divulgación científica ULL; 23 edition (23-AGO-1994)"+"\n"+
                               "ISBN-10: 66666"+"\n"+
                               "ISBN-12: 77777"+"\n")
    end
  end
end

